const jwt = require('jsonwebtoken');
const secret = ':a:YXfx$G!/!ziQ!UXupAr)2R(gr)V'

const verificarAuth = (req, res, next) => {
  // Leer headers
  let token = req.get('token')

  jwt.verify(token,secret, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        mensaje: 'Usuario no valido'
      })
    }

    req.usuario = decoded.data

    next()
  })
}

const verificarAdmin = (req, res, next) => {
  const rol = req.usuario.role

  if (rol == 'ADMIN') {
    next()
  }
  else {
    return res.status(401).json({
      mensaje: 'Usuario sin privilegios'
    })
  }
}

module.exports = { verificarAuth, verificarAdmin }
