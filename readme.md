# V2 Youtube - Back

_Proyecto realizado en curso de [udemy](https://www.udemy.com/course/curso-vue/) con bluuweb._

## Setup

```bash
# instalar dependencias
npm i

# Iniciar servidor de desarrollo con autoreload
npm run devbabel

# Iniciar modo produccion
npm run start

```

## Front del proyecto
[Front](https://gitlab.com/can359/cursovue-mevn_front)
