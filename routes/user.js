import express from 'express'
import User from '../models/user'
import { verificarAuth, verificarAdmin } from "../middlewares/auth";

const router = express.Router()
// Hash contrasena
const bcrypt = require('bcrypt');
const saltRounds = 10;
// Filtrar campos de PUT
const _ = require('underscore');

// Crear usuario
router.post('/user/new', async(req, res) => {
  const body = {
   nombre:  req.body.nombre,
   email: req.body.email,
   role: req.body.role,
  }
  body.pass = bcrypt.hashSync(req.body.pass, saltRounds)
  try {
    const usuarioDB = await User.create(body)
    res.json(usuarioDB)
  } catch (error) {
    return res.status(500).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

// Modificar usuario
router.put('/user/:id', [verificarAuth, verificarAdmin], async(req, res) => {
  const id = req.params.id
  const body = _.pick(req.body, ['nombre', 'email', 'activo', 'pass'])
  if (body.pass) {
    body.pass = bcrypt.hashSync(req.body.pass, saltRounds)
  }
  try {
    const usuarioDB = await User.findByIdAndUpdate(
      id,
      body,
      {new: true, runValidators: true}
    )
    res.json(usuarioDB)
  } catch (error) {
    return res.status(400).json({
      mensaje: 'Ocurrio un error',
      error
    })
  }
})

//

module.exports = router
