import mongoose from 'mongoose'
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator');

const roles = {
  values: ['ADMIN', 'USER'],
  message: 'Rol {VALUE} no valido'
}

const userSchema = new Schema({
  nombre: {
    type: String,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    unique: true,
    required: [true, 'El email es obligatorio']
  },
  pass: {
    type: String,
    required: [true, 'Pass es obligatoria']
  },
  date: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    default: 'USER',
    enum: roles
  },
  activo: {
    type: Boolean,
    default: true
  }
})

userSchema.plugin(uniqueValidator, { message: 'Error, el {PATH} ya esta siendo usado.' })

// Eliminar pass de respuesta JSON
userSchema.methods.toJSON = function () {
  const obj = this.toObject();
  delete obj.pass;
  return obj;
}

const User = mongoose.model('User', userSchema)

export default User
