import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import path from 'path' // Para acceder al directorio actual
import mongoose from 'mongoose' // Conexion a DB

const app = express()
const port = 3000

const uri = 'mongodb://localhost:27017/mevn-v2-youtube'
const options = { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true }
mongoose.connect(uri, options).then(
  () => { console.log('Conectado a mongoDB'); },
  err => { err }
);

// Middleware
app.use(morgan('tiny'))
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true })) // application/x-www-form-urlencoded

// Rutas
const api = '/api'
// app.get('/', (req, res) => {
//   res.send('Hello Moco!')
// })

app.use(api, require('./routes/nota'))
app.use(api, require('./routes/user'))
app.use(api + '/login', require('./routes/login'))

// Middleware para Vue.js router modo history
const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

app.set('puerto', process.env.PORT || 3000)
app.listen(app.get('puerto'), () => {
  console.log(`Ejemplo app escuchando en http://localhost:${app.get('puerto')}`)
})
